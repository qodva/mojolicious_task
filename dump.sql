-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `pass` varchar(32) DEFAULT NULL,
  `sex` enum('M','F','unknown') DEFAULT 'unknown',
  `money` decimal(10,2) DEFAULT NULL,
  `avatar` varchar(256) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'first_user','1@mail.net','069ef4cc94d7ab7328a96597244afe3f','unknown',542.00,NULL,'2017-01-12 10:55:05','2017-01-11 11:10:48'),(2,'second user','2@user.net','8810ffb17f4bd019aba45f214ee31fdc','unknown',546.00,NULL,'2017-01-12 10:55:05','2017-01-10 11:10:48'),(3,'third user','3@roma.net','77f8b80f09d2fc83c7e54f55a27d2a76','unknown',127.00,NULL,'2017-01-12 10:55:05','2017-01-09 11:10:48'),(4,'Jolly Rodger','4@pirates.net','368f338172767e06f7cc46c12fc6135f','unknown',4.00,NULL,'2017-01-12 10:55:05','2017-01-08 11:10:48'),(5,'Lucifer','5@life.net','9bfbb6c2885bdc1875ff315afd034f61','unknown',670.00,NULL,'2017-01-12 10:55:05','2017-01-07 11:10:48'),(6,'Terminator','6@sky.net','b420c1d31e69a823fbc30a434907c9c0','unknown',127.00,NULL,'2017-01-12 10:55:05','2017-01-06 11:10:48'),(7,'Poseidon','7@sea.net','0385fc7144c932a3d7b9dfca3351a9b5','unknown',946.00,NULL,'2017-01-12 10:55:05','2017-01-05 11:10:48'),(8,'Snake','8@sand.net','dfa90f1b4eb3affbd3b46af34ed2477c','unknown',28.00,NULL,'2017-01-12 10:55:05','2017-01-04 11:10:48'),(9,'Clock','9@time.net','98a892aea3359481ae30e7b66c44d7a8','unknown',536.00,NULL,'2017-01-12 10:55:05','2017-01-03 11:10:48'),(10,'Ceasar','10@roman.net','28efee6aa9d31d4780ac0ec5904f4f4e','unknown',NULL,NULL,'2017-01-13 06:34:17','2017-01-02 11:10:48'),(20,'edit_tester','edittest@mail.ru','cd40e6834016c70c38cbbc8e5754ee3f','unknown',1234.00,NULL,'2017-01-13 11:41:12','2017-01-13 11:20:33'),(21,'up_tester','up_tester@mail.ru','f7791f861892f8947bb537845e454e1f','unknown',NULL,'/avatars/32179069932_9e7b999a5f_q.jpg','2017-01-16 11:49:51','2017-01-16 11:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-16 16:28:46
