USE test;

ALTER TABLE users
ADD COLUMN sex ENUM('M', 'F', 'unknown') DEFAULT 'unknown' AFTER pass;