USE test;

SET SQL_SAFE_UPDATES = 0;


UPDATE users
SET `sum` = `sum` + 1
WHERE id = 1;


UPDATE users
SET `sum` = 42
WHERE email = '1@mail.net';


UPDATE users
SET `sum` = `sum` + 1
WHERE created BETWEEN NOW() - interval 1 month and NOW();


