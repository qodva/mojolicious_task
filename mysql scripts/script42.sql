USE test;

ALTER TABLE users
ADD COLUMN name VARCHAR(64) AFTER id;


INSERT INTO users values
	('1@mail.net', 'first_user'),
    ('2@user.net', 'second user'),
    ('3@roma.net', 'third user'),
    ('4@pirates.net', 'Jolly Rodger'),
    ('5@life.net', 'Lucifer'),
    ('6@sky.net', 'Terminator'),
    ('7@sea.net', 'Poseidon'),
    ('8@sand.net', 'Snake'),
    ('9@time.net', 'Clock'),
    ('10@roman.net', 'Ceasar')
    ON DUPLICATE KEY UPDATE name = values(name);
