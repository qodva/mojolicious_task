USE test;

INSERT INTO users(email, pass, `sum`, created) values
	('1@mail.net', md5('first user'), 42, NOW()),
    ('2@user.net', md5('second user'), 542, NOW() - INTERVAL 1 DAY),
    ('3@roma.net', md5('third user'), 123, NOW() - INTERVAL 2 DAY),
    ('4@pirates.net', md5('Jolly Rodger'), 0, NOW() - INTERVAL 3 DAY),
    ('5@life.net', md5('Lucifer'), 666, NOW() - INTERVAL 4 DAY),
    ('6@sky.net', md5('Terminator'), 123, NOW() - INTERVAL 5 DAY),
    ('7@sea.net', md5('Poseidon'), 942, NOW() - INTERVAL 6 DAY),
    ('8@sand.net', md5('Snake'), 24, NOW() - INTERVAL 7 DAY),
    ('9@time.net', md5('Clock'), 532, NOW() - INTERVAL 8 DAY),
    ('10@roman.net', md5('Ceasar'), 843, NOW() - INTERVAL 9 DAY);