use test;

ALTER TABLE users
CHANGE COLUMN `sum` `sum` DECIMAL(10, 2);
