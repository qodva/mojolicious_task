$().ready(function(){
    var searchBtnHandler = function(){
        var searchValue = document.querySelector('#search-value').value;
        window.location.href = "/users?q=" + searchValue;
    };

    $("#add-user-btn").click(function(){
        var url = window.location.protocol + "//" + window.location.host +
                  window.location.pathname;
        window.location.href = url + (url.endsWith('/') ? '' : '/') + "add";
    });
    $("#search-btn").click(searchBtnHandler);
    $("#search-value").keypress(function(e){
        if (e.which == 13 && this.value){
            searchBtnHandler();
        }
    });
});