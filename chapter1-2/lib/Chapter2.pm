package Chapter2;
use Mojo::Base 'Mojolicious';


use DBI;
use Cwd 'abs_path';
use File::Basename;
use File::Spec;

use DB;

has dbconn => sub {
    return $DB::dbconn;
};

has home_dir => sub {
    return dirname dirname abs_path __FILE__;
};

has media_root => sub {
    return "./media"
};


# This method will run once at server start
sub startup {
    my $self = shift;

    $self->secrets(['trololololololololololololo']);

    #  push @{$self->renderer->paths}, map({File::Spec->join($self->home_dir, $_)} @TEMPLATE_DIRS);
    push @{$self->renderer->paths}, ("./tmpl");
    push @{$self->static->paths}, ("./data");
    push @{$self->static->paths}, $self->media_root;

    # Documentation browser under "/perldoc"
    $self->plugin('PODRenderer');
    $self->plugin('Chapter2::Helpers::Auth');
    $self->plugin('Chapter2::Helpers::Media');

    # Router
    my $r = $self->routes;

    # Users routes.


    $r->get('/login')->to('users#auth');
    $r->post('/login')->to('users#auth_post');
    $r->get('/logout')->to('users#logout');
    $r->get('/api/users')->to('users#api');

    my $check_user_auth = $r->under('/')->to('users#check_auth');

    $check_user_auth->get('/users')->to('users#list');
    $check_user_auth->get('/users/add')->to('users#add');
    $check_user_auth->post('/users/add')->to('users#add_post');
    $check_user_auth->get('/users/:id/remove' => [id => qr/\d+/])->to('users#delete');
    $check_user_auth->get('/users/:id/edit' => [id => qr/\d+/])->to('users#edit');
    $check_user_auth->post('/users/:id/edit' => [id => qr/\d+/])->to('users#edit_post');
}

1;
