package DB;
use strict;
use warnings FATAL => 'all';

use DBI;


my $DB_NAME = 'test';
my $USER = 'root';
my $PWD = 'qwerty12+';

my $conn_str = "DBI:mysql:database=$DB_NAME;";
our $dbconn = DBI->connect($conn_str, $USER, $PWD) or die 'Can not connect to database.';

1;