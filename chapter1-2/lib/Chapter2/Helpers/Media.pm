package Chapter2::Helpers::Media;
use base 'Mojolicious::Plugin';

use File::Spec;
use Data::Dumper;
use v5.18;

sub register {
    my ($self, $app) = @_;
    $app->helper(upload_file_path => \&_get_upload_file_path);
    $app->helper(save_upload_file => \&_save_upload_file);
    $app->helper(get_file_path => \&_get_file_path)
}

sub _get_upload_file_path {
    my ($c, $sub_dir, $filename) = @_;
    my $upload_file_data = {};
    my $media_root = $c->app->media_root;
    $upload_file_data->{file_path} = File::Spec->join($media_root, $sub_dir, $filename);
    $upload_file_data->{file_link} = File::Spec->join('/', $sub_dir, $filename);

    return $upload_file_data;
}

sub _save_upload_file {
    my ($c, $sub_dir, $upload) = @_;
    my $upload_file_path = $c->upload_file_path($sub_dir, $upload->filename);
    $upload->move_to($upload_file_path->{file_path});
    return $upload_file_path->{file_link};
}

sub _get_file_path {
    my ($c, $link) = @_;
    return File::Spec->join($c->app->media_root, $link);
}

1;