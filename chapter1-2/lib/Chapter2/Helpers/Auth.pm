package Chapter2::Helpers::Auth;
use base 'Mojolicious::Plugin';

use v5.18;
use Data::Dumper;

sub register {
    my ($self, $app) = @_;
    $app->helper(logged => \&_logged);
    $app->helper(user => \&_user);
}

sub _logged {
    my ($c, $user_id) = @_;
    my $state = undef;
    my $user = _check_user_id($c, $user_id);
    if ($user) {
        $c->stash->{user} = $user;
        $state = 1;
    }
    return $state;
}

sub _user {
    my $c = shift;
    return $c->stash->{user};
}

sub _check_user_id {
    my ($c, $user_id) = @_;

    return undef unless(defined $user_id);

    my $query = $c->app->dbconn->prepare("Select * from users "
                                         . "Where id=?;");
    $query->bind_param(1, $user_id);
    $query->execute;
    return $query->fetchrow_hashref;
}

1;