package Chapter2::Controller::Users;
use Mojo::Base 'Mojolicious::Controller';
use Mojolicious::Validator;
use Mojo::JSON qw(encode_json);

use Digest::MD5 qw(md5_hex);
use DBI qw(:sql_types);
use Chapter2::Models::User;

use Data::Dumper;

my $LIST_ORDER = [qw(id avatar name email money updated created)];
my $COLUMNS_CAPTIONS = {
    id      => 'ID',
    name    => 'Имя',
    email   => 'Электронная почта',
    money   => 'Деньги',
    updated => 'Дата последнего редактирования',
    created => 'Дата создания',
    avatar  => 'Аватар'
};

sub api {
    my $self = shift;

    my $search_value = $self->param( 'q' );
    my $search_clause = {
        name__like  => "%$search_value%",
        email__like => "%$search_value%"
    };

    my $users = Chapter2::Models::User->search($search_clause, $LIST_ORDER);

    $self->render(json => {status => 'ok', list => $users});
}


sub auth {
    my $self = shift;
    $self->render('users/login');
}

sub auth_post {
    my $self = shift;
    my $login = $self->param('login');
    my $pass = $self->param('pass');
    $pass = md5_hex($pass);
    my $user_data = Chapter2::Models::User->get({name=>$login, pass=>$pass});
    my $status = undef;

    if (defined $user_data){
        $self->session->{user_id} = $user_data->{id};
        $status = 1;
        $self->redirect_to('/users');
    } else {
        if (Chapter2::Models::User->get({name=>$login})) {
            $self->validation->error('pass' => ['Invalid password.']);
        }
        else {
            $self->validation->error('login' => ['User not found.']);
        }
        $self->render('users/login');
    }
    return $status;
}

sub logout {
    my $self = shift;
    delete $self->session->{user_id};
    delete $self->stash->{user};
    $self->redirect_to('/login', previous=>"test");
}


sub check_auth {
    my $self = shift;

    my $user_id = $self->session->{user_id};
    return 1 if ($self->logged($user_id));

    delete $self->session->{user_id};
    $self->redirect_to('/login');
    undef;
}

sub list {
    my $self = shift;

    my $search_value = $self->param('q');
    my $search_clause = {
        name__like  => "%$search_value%",
        email__like => "%$search_value%"
    };

    my $users = Chapter2::Models::User->search($search_clause, $LIST_ORDER);

    $self->render(
        users        => $users,
        col_captions => $COLUMNS_CAPTIONS,
        list_order   => $LIST_ORDER,
        search_value => $search_value
    );
}

sub add {
    my $self = shift;

    $self->render('users/form', btn_name => 'Добавить',
                  attrs => {}, title=> 'Добавление пользователя');
}

sub add_post {
    my $self = shift;
    my $validation = $self->validation;
    my $params = $self->_get_user_params;


    $validation->required('name');
    $validation->required('email')->like(qr/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/);
    $validation->required('pass')->size(6, 'Inf');
    $validation->optional('money')->like(qr/^\d+\.?\d*|$/);

    unless ($self->check_unique_email($params->{email})){
        $validation->error('email'=> ['unique', 'must be unique']);
    }

    if ($params->{avatar} && !($params->{avatar}->filename =~ /.*\.png|jpg/)){
        $validation->error('avatar'=> ['extension', 'file extension must be png or jpg.']);
    }
    if ($validation->has_error){
        $self->add();
    } else{
        $self->insert($params);
        $self->flash(message => 'Пользователь успешно добавлен.');
        $self->redirect_to("/users");
    }
}

sub edit {
    my $self = shift;
    my $id = $self->param('id');
    $self->render('users/form',
        btn_name => 'Изменить',
        attrs    => Chapter2::Models::User->get({id => $id}),
        title    => 'Изменение пользователя'
    );
}

sub edit_post {
    my $self = shift;
    my $validation = $self->validation;
    my $params = $self->_get_user_params;
    my $id = $self->param('id');
    if (!$params->{pass}){
        delete $params->{pass};
    }

    $validation->required('name');
    $validation->required('email')->like(qr/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/);
    $validation->optional('pass')->size(6, 'Inf');
    $validation->optional('money')->like(qr/^\d+\.?\d*|$/);
    unless ($self->check_unique_email($params->{email}, $id)){
        $validation->error('email'=> ['unique', 'must be unique']);
    }
    if ($params->{avatar} && !($params->{avatar}->filename =~ /.*\.png|jpg/)){
        $validation->error('avatar'=> ['extension', 'file extension must be png or jpg.']);
    }

    if ($validation->has_error){
        $self->edit();
    } else {
        $self->update($params);;
        $self->flash(message => 'Пользователь успешно изменен.');
        $self->redirect_to("/users");
    }
}

sub delete {
    my $self = shift;
    my $id = $self->param('id');
    if (Chapter2::Models::User->delete({id => $id})){
        $self->flash(message => 'Пользователь успешно удален');
    };
    $self->redirect_to("/users");
}

sub insert {
    my ($self, $attrs) = @_;
    $attrs->{pass} = md5_hex($attrs->{pass});
    if ($attrs->{avatar}){
        $attrs->{avatar} = $self->save_upload_file('avatars', $attrs->{avatar});
    }
    return Chapter2::Models::User->insert($attrs);
}

sub update {
    my ($self, $attrs) = @_;
    if ($attrs->{pass}){
        $attrs->{pass} = md5_hex($attrs->{pass});
    }
    if ($attrs->{avatar}){
        $attrs->{avatar} = $self->save_upload_file('avatars', $attrs->{avatar});
    }
    return Chapter2::Models::User->update($attrs, {id => $self->param('id')});
}

sub check_unique_email {
    my ($self, $email, $id) = @_;
    my $clause_params = { email => $email};
    if ($id){
        $clause_params->{id__ne} = $id;
    }
    return !Chapter2::Models::User->get($clause_params);
}

sub _get_user_params {
    my $self = shift;
    my $params = {};

    $params->{name} = $self->req->param('name');
    $params->{pass} = $self->req->param('pass');
    my $money = $self->req->param('money');
    unless ($money eq ''){
        $params->{money} = $money;
    }
    $params->{email} = $self->req->param('email');
    my $avatar = $self->req->upload('avatar');
    if ($avatar->size){
        $params->{avatar} = $avatar;
    }

    say Dumper($params);

    return $params;
}


1;