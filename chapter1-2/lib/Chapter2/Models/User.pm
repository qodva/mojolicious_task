package Chapter2::Models::User;

use Mojo::Base;

use DBI;
use DB;

use Data::Dumper;
use v5.18;

my $TABLE = 'users';


sub get {
    my ($cls, $clause_attrs, $columns) = @_;
    my $query = $cls->_get_select_query($clause_attrs, $columns);
    $query->execute;
    return $query->fetchrow_hashref;
}

sub where {
    my ($cls, $clause_attrs, $columns) = @_;
    my $query = $cls->_get_select_query($clause_attrs, $columns);
    $query->execute;
    my $data = [];
    while (my $row = $query->fetchrow_hashref){
        push @$data, $row;
    }
    return $data;
}

sub search {
    my ($cls, $clause_attrs) = @_;
    my $query = $cls->_get_select_query($clause_attrs, undef, 'or');
    $query->execute;
    my $data = [];
    while (my $row = $query->fetchrow_hashref){
        push @$data, $row;
    }
    return $data;
}

sub update {
    my ($cls, $attr_values, $clause_attrs) = @_;
    say Dumper($attr_values, $clause_attrs);
    return $cls->_get_update_query($attr_values, $clause_attrs)->execute;
}

sub delete {
    my ($cls, $clause_attrs) = @_;
    return $cls->_get_delete_query($clause_attrs)->execute;
}

sub insert {
    my ($cls, $attr_values) = @_;
    return $cls->_get_insert_query($attr_values)->execute;
}

sub _get_select_query {
    my ($cls, $clause_attrs, $columns, $logical_operation) = @_;
    my $query_text = "Select ";
    $logical_operation = $logical_operation || 'and';
    if (defined $columns){
        $query_text .=  join ", ", @$columns;
    } else {
        $query_text .= " * ";
    }

    $query_text .= " From $TABLE" . $cls->_get_where_clause($clause_attrs, $logical_operation);

    say $query_text;

    my $query = $DB::dbconn->prepare($query_text);
    my $arg_position = 1;
    if ($clause_attrs){
        for (values %$clause_attrs){
            $query->bind_param($arg_position++, $_);
        }
    }
    return $query;
}

sub _get_delete_query {
    my ($cls, $clause_attrs) = @_;
    my $query_text = "DELETE FROM $TABLE"
                     . $cls->_get_where_clause($clause_attrs);
    my $query = $DB::dbconn->prepare($query_text);
    my $arg_position = 1;
    if ($clause_attrs){
        for (values %$clause_attrs){
            $query->bind_param($arg_position++, $_);
        }
    }
    return $query;
}

sub _get_update_query {
    my ($cls, $values, $clause_attrs) = @_;
    return undef unless(defined $values);

    my $query_text = "Update $TABLE "
                    . $cls->_get_set_clause($values)
                    . $cls->_get_where_clause($clause_attrs);
    my $query = $DB::dbconn->prepare($query_text);
    say Dumper($query_text);
    my $arg_position = 1;
    $clause_attrs = $clause_attrs || {};
    for (values %$values, values %$clause_attrs){
        say $_;
        $query->bind_param($arg_position++, $_);
    }
    return $query;
}

sub _get_insert_query {
    my ($cls, $attr_values) = @_;
    say Dumper($attr_values);
    return undef unless (defined $attr_values);

    my $query_text = "INSERT INTO $TABLE("
                     . join(', ', keys %$attr_values)
                     . ') values('
                     . join(', ', ('?') x scalar(keys(%$attr_values)))
                     . ');';
    say $query_text;
    my $query = $DB::dbconn->prepare($query_text);
    my $arg_position = 1;
    for (values %$attr_values){
        $query->bind_param($arg_position++, $_);
    }
    return $query;
}

sub _get_where_clause {
    my ($cls, $clause_attrs, $logical_operation) = @_;
    return ';' unless (defined $clause_attrs);
    $logical_operation = $logical_operation || 'and';

    my $where_clause = ' WHERE ';
    $where_clause .= join( " $logical_operation ", map {$cls->_transform_field_lookup($_)} keys %$clause_attrs);
    return $where_clause;
}

sub _get_set_clause {
    my ($cls, $clause_attrs) = @_;
    return ';' unless (defined $clause_attrs);

    my $set_clause = ' SET ';
    $set_clause .= join( ', ', map {"$_ = ?"} keys %$clause_attrs);
    return $set_clause;
}

sub _transform_field_lookup {
    my ($cls, $lookup) = @_;
    my ($column, $operation) = split '__', $lookup;
    my $sql_expr;
    print Dumper($lookup);
    given($operation){
        when ('like')   {$sql_expr = "$column LIKE ?";}
        when ('ne')     {$sql_expr = "$column != ?";}
        default         {$sql_expr = "$column = ?";}
    }
    return $sql_expr;
}

1;