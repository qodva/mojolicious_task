use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

use Mojo::JSON qw(decode_json);

my $t = Test::Mojo->new('Chapter2');
$t->get_ok('/api/users')->status_is(200);

my $ua = $t->ua;

my $content = decode_json $ua->get('/api/users')->result->body;

done_testing();
