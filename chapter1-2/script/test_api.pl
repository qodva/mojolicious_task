use strict;
use warnings FATAL => 'all';

use Mojo::UserAgent;
use Mojo::JSON qw(decode_json);

use Data::Dumper;


my $user_agent = Mojo::UserAgent->new;
print Dumper(decode_json($user_agent->get('http://127.0.0.1:3000/api/users')->result->body));

