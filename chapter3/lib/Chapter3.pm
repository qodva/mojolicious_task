package Chapter3;
use Mojo::Base 'Mojolicious';

use File::Basename;

use Cwd 'abs_path';
use File::Spec;

my @TEMPLATE_DIRS = qw(tmpl);
my @STATIC_DIRS = qw(data);

sub startup {
  my $self = shift;
  my $base_dir = dirname dirname abs_path __FILE__;

  push @{$self->renderer->paths}, map({File::Spec->join($base_dir, $_)} @TEMPLATE_DIRS);
  push @{$self->static->paths}, map({File::Spec->join($base_dir, $_)} @STATIC_DIRS);

  # Documentation browser under "/perldoc"
  $self -> plugin('PODRenderer');

  $self -> inactivity_timeout(60 * 60);

  # Router
  my $r = $self->routes;

  $r->get('/square')->to('square#square');
  $r->get('/control')->to('square#control');
  $r->websocket('/socket')->to('square#socket');
}

1;
