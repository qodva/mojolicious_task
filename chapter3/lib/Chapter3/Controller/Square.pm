package Chapter3::Controller::Square;
use Mojo::Base 'Mojolicious::Controller';

use Data::Dumper;

my $clients = {};

sub square {
    my $self = shift;
    $self->render();
}

sub control {
    my $self = shift;
    $self->render();
}

sub socket {
    my $self = shift;

    my $client_id = $self->tx;
    $clients->{$client_id} = $client_id;

    $self->on(message => sub {
        my ($self, $msg) = @_;
        for (keys %$clients){
            $clients->{$_}->send($msg);
        }
    });

    $self->on(finish => sub {
        delete $clients->{$client_id};
    });
}

1;

