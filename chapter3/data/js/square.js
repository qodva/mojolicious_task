document.addEventListener("DOMContentLoaded", function(){
    var square = document.querySelector(".square");
    var ws = new WebSocket("ws://" + window.location.host + "/socket");
    ws.onmessage = function(){
        square.classList.toggle("first");
        square.classList.toggle("second");
    };
});